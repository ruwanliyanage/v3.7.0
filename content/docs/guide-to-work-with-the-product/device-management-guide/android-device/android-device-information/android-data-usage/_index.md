---
bookCollapseSection: true
weight: 3
---

# Android Data Usage

This feature enables the measure of data usage of the devices over the past 30 days. It distinctly measures the usage of Mobile data and WiFi separately. The users can view this under the Device Information of the Agent.

<!-- ![image](1.png) -->
<img src="1.png" width="300" style="border:1px solid #ccc">

The data usage information can be accessed inside the database under the fields **WIFI_DATA_USAGE** and **MOBILE_DATA_USAGE**.