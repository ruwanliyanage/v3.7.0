---
bookCollapseSection: true
weight: 1
---

# <strong> Passcode Policy </strong>

{{< hint info >}}
<b> <a href ="https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This configuration can be used to set a passcode policy to an iOS Device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Force Passcode</strong></td>
            <td>Determines whether the user is forced to set a PIN. Simply setting this value (and not others) forces the user to enter a passcode, without imposing a length or quality
            </td>
        </tr>
        <tr>
            <td><strong>Allow Simple Value</strong></td>
            <td> Determines whether a simple passcode is allowed. A simple passcode is defined as containing repeated characters, or increasing/decreasing characters (such as 123 or CBA). Setting this value to false is synonymous to setting minComplexChars to ”1”.</td>
        </tr>
        <tr>
            <td><strong>Allow Alphanumeric Value</strong></td>
            <td> Specifies whether the user must also enter alphabetic characters (”abcd”) along with numbers, or if numbers only are sufficient.
            </td>
        </tr>
        <tr>
            <td><strong>Minimum passcode length</strong></td>
            <td>Specifies the minimum overall length of the passcode.
            </td>
        </tr>
        <tr>
            <td><strong>Minimum number of complex characters</strong></td>
            <td>Specifies the minimum number of complex characters that a passcode must contain. A ”complex” character is a character other than a number or a letter, such as &%$#.
                <br>( Should be in between 1-to-730 days or none )</td>
        </tr>
        <tr>
            <td><strong>Passcode history</strong></td>
            <td> When the user changes the passcode, it has to be unique within the last N entries in the history. Minimum value is 1, maximum value is 50.
                <br>( Should be in between 1-to-50 passcodes or none )</td>
        </tr>
        <tr>
            <td><strong>Auto Lock Time in minutes</strong></td>
            <td>Specifies the maximum number of minutes for which the device can be idle (without being unlocked by the user) before it gets locked by the system. Once this limit is reached, the device is locked and the passcode must be entered. The user can edit this setting, but the value cannot exceed the maxInactivity value.</td>
        </tr>
        <tr>
            <td><strong>Grace period in minutes for device lock</strong></td>
            <td> The maximum grace period, in minutes, to unlock without entering a passcode. Default is 0, that is no grace period, which requires entering a passcode immediately.
            </td>
        </tr>
        <tr>
            <td><strong>Maximum number of failed attempts</strong></td>
            <td> Allowed range [2...11]. Specifies the number of allowed failed attempts to enter the passcode at the deviceʼs lock screen. After six failed attempts, there is a time delay imposed before a passcode can be entered again. The delay increases with each attempt.Once this number is exceeded,on iOS the device is wiped.</td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "https://entgra-documentation.gitlab.io/v3.7.0/docs/guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}